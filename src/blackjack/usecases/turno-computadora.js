import Swal from 'sweetalert2';
import { pedirCarta, valorCarta, crearCartaHTML } from "./index";

const puntosTotales = document.querySelectorAll('span');

let totalJugador = 0,
    totalComputadora = 0;

/**
 * Función para turno de la computadora
 * @param { Number } puntosMinimos puntos mínimos que la computadora necesita para ganar
 * @param { Array<String> } deck
 * @param { HTMLElement } puntosHTML elemento HTML para mostrar los puntos
 * @param { HTMLElement } divCartasComputadora elemento HTML para mostrar las cartas
 * @returns { void }
 */
export const turnoComputadora = ( puntosMinimos, puntosHTML, divCartasComputadora, deck = [] ) => {

    if( !puntosMinimos ) throw new Error('Puntos mínimos son necesarios');

    let puntosComputadora = 0;

    do {
        const carta = pedirCarta(deck);

        puntosComputadora = puntosComputadora + valorCarta( carta );
        puntosHTML.innerText = puntosComputadora;

        // <img class="carta" src="assets/cartas/2C.png">
        const imgCarta = crearCartaHTML(carta);

        divCartasComputadora.append( imgCarta );

        if( puntosMinimos > 21 ) {
            break;
        }

    } while(  (puntosComputadora < puntosMinimos)  && (puntosMinimos <= 21 ) );

    setTimeout(() => {
        if(puntosMinimos > 21){
            totalComputadora++;
            puntosTotales[1].innerText = totalComputadora;
            Swal.fire("Gano la computadora");
        } else if(puntosComputadora > 21){
            totalJugador++;
            puntosTotales[0].innerText = totalJugador;
            Swal.fire("Gano el jugador");
        } else if(puntosMinimos === puntosComputadora){
            Swal.fire("Empate, nadie gana");
        }else if(puntosMinimos > puntosComputadora){
            totalJugador++;
            puntosTotales[0].innerText = totalJugador;
            Swal.fire("Gano el jugador");
        } else{
            totalComputadora++;
            puntosTotales[1].innerText = totalComputadora;
            Swal.fire("Gano la computadora");
        }
    }, 1000 );
}