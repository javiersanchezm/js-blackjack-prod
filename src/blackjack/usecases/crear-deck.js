import _ from 'underscore';

/**
 * Está función crea un nuevo deck
 * @param {array<String>} tiposDeCarta ejemplo: ['C','D','H','S'];
 * @param {array<String>} tiposEspeciales ejemplo: ['A','J','Q','K'];
 * @returns {array<String>} regresa un nuevo deck de cartas
 */
export const crearDeck = (tiposDeCarta, tiposEspeciales) => {

    //Es typeScript no es necesario
    if( !tiposDeCarta || tiposDeCarta.lengtth === 0 ) throw new Error('TiposDeCarta es obligatorio como un arreglo de string');
    if( !tiposEspeciales || tiposEspeciales.lengtth === 0 ) throw new Error('TiposEspcialestiposEspeciales es obligatorio como un arreglo de string');

    let deck = [];

    for(let i = 2; i <= 10; i++){
        for(let tipo of tiposDeCarta){
            deck.push(i + tipo);
        }
    }

    for( let tipo of tiposDeCarta ){
        for( let esp of tiposEspeciales){
            deck.push( esp + tipo );
        }
    }

    deck = _.shuffle(deck);

    return deck;
}

//export default crearDeck;