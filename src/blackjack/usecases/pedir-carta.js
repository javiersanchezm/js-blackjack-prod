/**
 * Función permite tomar una carta
 * @param {array<String>} deck Todas las cartas es un arreglo de string
 * @returns {String} regresa una carta del deck
 */

export const pedirCarta = (deck) => {

    if ( !deck || deck.length === 0 ) {
        throw 'No hay cartas en el deck';
    }
    const carta = deck.pop();
    return carta;
}