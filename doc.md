### Migrar proyecto a Vite
- Copiar todo lo necesario para que funcione HTML, CSS, JS
- Instalar dependencias del proyecto [underscore] y [sweetalert2]
- Carpeta public en Vite para todos los recursos estáticos como la carpeta assets
- Refactorización. Cambiar la forma en que se ejecuta el código pero el resultado en exactamente el mismo.
    - módulo. Código encapsulado que nos permite su reutilización fácilmente.
    - Separar en archivos con responsabilidades únicas (Clean Code).
    - La estructura de directorios queda a discreción del programador o arquitectos de software que decidan que estructura o patrón de diseño quieren utilizar.
    - Cuando se copia y se pega código se puede refactorizar y hacerlo mejor mediante una función.
- Documentación para JavaScript. [jsdoc.app], otros desarrolladores tengan noción de como funcionan las cosas agregando documentación y validaciones así nos aseguramos que funcione correctamente, atajo [/**enter]
```javascript
/**
 * Está función crea un nuevo deck
 * @param {array<String>} tiposDeCarta ejemplo: ['C','D','H','S'];
 * @param {array<String>} tiposEspeciales ejemplo: ['A','J','Q','K'];
 * @returns {array<String>} regresa un nuevo deck de cartas
 */
```

### Todo el proyecto se trabajo con módulos cada uno de los archivos "usecases" es un módulo, y trabajar de está forma que realizan tareas específicas es más fácil de trabajar y de mantener.

### Aplicación usando NodeJS con módulos, y al realizar el bundler de la App se compacta y termina siendo unificado en un solo archivo, minificado y hace muy difícil leer el código y poder reconstruirlo y ese build no tiene código sencible para los usuarios.